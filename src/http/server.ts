import https from 'http';
import express from 'express';

import config from '../config';
import router from './routes';

const { env } = process;

const {
  server: { PORT }
} = config;

const startServer = async (): Promise<void> =>{
  try {
    const app = express();

    console.log(`Starting http server on port: ${PORT}.`);

    app.use(router);

    https.createServer(app).listen(PORT);

  } catch (e) {
    console.error(`Failed to start server:`, e.message, e.stack);
    process.exit();
  }
};

export default startServer;
