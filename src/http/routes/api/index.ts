import { Router } from 'express';

import protectedRoutes from './protected';
import publicRoutes from './public';

const router = Router();

router.use(protectedRoutes);
router.use(publicRoutes);

export default router;
