import { Request, Response } from "express";

export default async (req: Request, res: Response): Promise<any> => {
  return res.status(200).json({status: "OK"});
}
