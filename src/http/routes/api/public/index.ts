import { Router } from 'express';

import healthcheck from './healthCheck';

const router = Router();

router.get("/healthcheck", healthcheck);

export default router;
