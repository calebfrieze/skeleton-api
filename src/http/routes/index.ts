import { Router } from 'express';

import apiRoutes from './api';
import docRoutes from './docs';

const router = Router();

router.use(apiRoutes);
router.use('/api-docs', docRoutes);

export default router;
