import aws from './aws';
const { env } = process;

const PORT = env.PORT ? parseInt(env.PORT, 10) : 9001;

const config = {
  ...aws,
  server: {
    HOST: env.HOST || '127.0.0.1',
    PORT
  }
};

export default config;
