const { env } = process;
const aws = {
  region: env.AWS_REGION || "us-west-2",
  endpoint: env.AWS_DYNAMODB_ENDPOINT || "http://localhost:9432",
  accessKeyId: env.AWS_ACCESS_KEY_ID || "AWS_ACCESS_KEY_ID",
  secretAccessKey: env.AWS_SECRET_ACCESS_KEY || "AWS_SECRET_ACCESS_KEY"
}

export default aws;
