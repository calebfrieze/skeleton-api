const awsConfig = require('../aws.config');
const AWS = require('aws-sdk');

AWS.config.update(awsConfig);

const DynamoDB = new AWS.DynamoDB();
const documentClient = new AWS.DynamoDB.DocumentClient();

const TableName = 'sample_table';
const tableParams = {
  TableName,
  AttributeDefinitions: [
    {
      AttributeName: 'id',
      AttributeType: 'N'
    }
  ],
  KeySchema: [
    {
      AttributeName: 'id',
      KeyType: 'HASH'
    }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 2,
    WriteCapacityUnits: 2
  },
  StreamSpecification: {
    StreamEnabled: false
  }
};

const params = {
  TableName,
  Item: {
    'id': 0,
    'name': 'Test Name'
  }
};

(async function runMigration() {
  await DynamoDB.createTable(tableParams, (err, data) => {
    if (err) {
      console.log(`Error creating table. Error ${err}`)
    } else {
      console.log(`Created table. Description: ${JSON.stringify(data, null, 2)}`);
    }
  });
  await documentClient.put(params, function (err, data) {
    if (err) {
      console.log("Error", err);
    } else {
      console.log("Success", data);
    }
  });
})();
