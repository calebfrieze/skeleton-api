const { env } = process;

const awsConfig = require('../aws.config');
const AWS = require('aws-sdk');

AWS.config.update(awsConfig);

const db = new AWS.DynamoDB();

const params = {
  TableName: "sample_table"
}

db.deleteTable(params, (err, data) => {
  if (err) {
    console.log(`Unable to delete table. Error: ${JSON.stringify(err, null, 2)}`);
  } else {
    console.log(`Deleted ${data.TableDescription.TableName}`);
  }
});
